package org.petza.tracker.service;

import org.junit.Test;
import org.petza.tracker.domain.Currency;
import org.petza.tracker.domain.NetAmounts;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ReadFileServiceTest {

    @Test
    public void testReadFile() {

        Map<String, Double> currencyUSDFxRates = new HashMap<>();
        currencyUSDFxRates.put("CZK", 20.0);
        currencyUSDFxRates.put("EUR", 0.8);
        currencyUSDFxRates.put("CHF", 0.95);
        currencyUSDFxRates.put("DKK", 6.00);
        currencyUSDFxRates.put("JPY", 105.09);
        currencyUSDFxRates.put("RUB", 56.86);
        currencyUSDFxRates.put("PLN", 3.41);

        NetAmounts netAmounts = new NetAmounts();
        ReadFileService readFileService = new ReadFileService("src/main/resources/amounts.txt", netAmounts, currencyUSDFxRates);

        try {
            readFileService.processLineByLine();
        } catch (IOException e) {
            System.out.println("Something went wrong during file processing skipping the file processing.");
            e.printStackTrace();
        }

        assertEquals(new BigDecimal(100), netAmounts.get(new Currency("USD", 1.0)));
        assertEquals(100.0, ((BigDecimal) netAmounts.get(new Currency("USD", 1.0))).doubleValue() / 1.0, 0.0001);

        assertEquals(new BigDecimal(200), netAmounts.get(new Currency("CZK", 20.0)));
        assertEquals(10.0, ((BigDecimal) netAmounts.get(new Currency("CZK", 20.0))).doubleValue() / 20.0, 0.0001);
        assertEquals(new BigDecimal(10.0).stripTrailingZeros(), ((BigDecimal) netAmounts.get(new Currency("CZK", 20.0))).divide(new BigDecimal(20.0), 2, RoundingMode.HALF_UP).stripTrailingZeros());

        assertEquals(new BigDecimal(300), netAmounts.get(new Currency("EUR", 0.8)));
        assertEquals(375.0, ((BigDecimal) netAmounts.get(new Currency("EUR", 0.8))).doubleValue() / 0.8, 0.0001);
        assertEquals(new BigDecimal(375).stripTrailingZeros(), ((BigDecimal) netAmounts.get(new Currency("EUR", 0.8))).divide(new BigDecimal(0.8), 2, RoundingMode.HALF_UP).stripTrailingZeros());
    }
}
