package org.petza.tracker.service;

import org.junit.Before;
import org.junit.Test;
import org.petza.tracker.domain.Currency;
import org.petza.tracker.domain.NetAmounts;
import org.petza.tracker.domain.Payment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ReadDataServiceTest {

    List<Payment> paymentList;

    @Before
    public void initValues() {

        paymentList = new ArrayList<>();
        paymentList.add(new Payment(new Currency("CZK", 20d), new BigDecimal(1000)));
        paymentList.add(new Payment(new Currency("USD", 1d), new BigDecimal(2000)));
        paymentList.add(new Payment(new Currency("EUR", 0.8d), new BigDecimal(3000)));
        paymentList.add(new Payment(new Currency("HKD", 1.5d), new BigDecimal(4000)));
        paymentList.add(new Payment(new Currency("CZK", 20d), new BigDecimal(-3000)));
        paymentList.add(new Payment(new Currency("EUR", 0.8d), new BigDecimal(500)));
    }

    @Test
    public void testStorePayment() {

        NetAmounts netAmounts = new NetAmounts();
        ReadDataService readDataService = new ReadDataService(netAmounts, "readDataService");
        paymentList.forEach(payment -> readDataService.storePayment(payment));

        assertEquals(new BigDecimal(-2000), netAmounts.get(new Currency("CZK", 20d)));
        assertEquals(new BigDecimal(2000), netAmounts.get(new Currency("USD", 1d)));
        assertEquals(new BigDecimal(3500), netAmounts.get(new Currency("EUR", 0.8d)));
        assertEquals(new BigDecimal(4000), netAmounts.get(new Currency("HKD", 1.5d)));
    }

}
