package org.petza.tracker;

import org.petza.tracker.domain.NetAmounts;
import org.petza.tracker.service.ReadFileService;
import org.petza.tracker.service.ReadDataService;
import org.petza.tracker.service.NetAmountsService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Main class
 *
 */
public class App {

    public static void main( String[] args ) {

        Map<String, Double> currencyUSDFxRates = new HashMap<>();

        currencyUSDFxRates.put("CZK", 20.54);
        currencyUSDFxRates.put("EUR", 0.81);
        currencyUSDFxRates.put("CHF", 0.95);
        currencyUSDFxRates.put("DKK", 6.00);
        currencyUSDFxRates.put("JPY", 105.09);
        currencyUSDFxRates.put("RUB", 56.86);
        currencyUSDFxRates.put("PLN", 3.41);

        NetAmounts netAmounts = new NetAmounts();

        if (args.length == 0 || args[0] == null || args[0].trim().isEmpty()) {
            System.out.println("No file path specified, going for manual processing.");
        } else {
            String filePath = args[0];
            ReadFileService readFileService = new ReadFileService(filePath, netAmounts, currencyUSDFxRates);
            try {
                readFileService.processLineByLine();
                System.out.println("File reading done.");
            } catch (IOException e) {
                System.out.println("Something went wrong during file processing skipping the file processing.");
            }
        }

        NetAmountsService netAmountsService = new NetAmountsService(netAmounts, "writeNetAmountsThread");
        ReadDataService readDataService = new ReadDataService(netAmounts, currencyUSDFxRates, "readDataThread", netAmountsService);

        readDataService.start();
        netAmountsService.start();
    }
}
