package org.petza.tracker.domain;

import lombok.Data;

@Data
public class Currency {

    private String currencyCode;
    private Double fxRate;

    public Currency() {
    }

    public Currency(String currencyCode, Double fxRate) {
        this.currencyCode = currencyCode;
        this.fxRate = fxRate;
    }
}
