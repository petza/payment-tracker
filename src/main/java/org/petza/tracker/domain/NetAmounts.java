package org.petza.tracker.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

@Data
public class NetAmounts extends ConcurrentHashMap {

    private ConcurrentHashMap<Currency, BigDecimal> values;
}
