package org.petza.tracker.domain;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Payment {

    private Currency   currency;
    private BigDecimal amount;

    public Payment() {
    }

    public Payment(Currency currency, BigDecimal amount) {
        this.currency = currency;
        this.amount = amount;
    }
}
