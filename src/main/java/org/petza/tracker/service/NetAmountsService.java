package org.petza.tracker.service;

import org.petza.tracker.domain.Currency;
import org.petza.tracker.domain.NetAmounts;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.atomic.AtomicBoolean;

public class NetAmountsService implements Runnable {

    private Thread netAmountsThread;

    private String name;
    private NetAmounts netAmounts;
    private final AtomicBoolean running = new AtomicBoolean(true);

    public NetAmountsService(NetAmounts netAmounts, String name)
    {
        this.netAmounts = netAmounts;
        this.name = name;
    }

    public void start() {
        netAmountsThread = new Thread(this);
        netAmountsThread.start();
    }

    public void stop() {
        running.set(false);
    }

    public void interrupt() {
        running.set(false);
        netAmountsThread.interrupt();
    }

    boolean isRunning() {
        return running.get();
    }

    @Override
    public void run() {

//        System.out.println(name + " starting.");

        while (isRunning()) {
            try {
                Thread.sleep(1*60*1000); // sleep for 1 minute

                if (netAmounts.size() > 0) {
                    printNetAmounts();
                }
            } catch (InterruptedException exc) {
                System.out.println(name + " interrupted.");
                printNetAmounts();
                interrupt();
            }
        }
        System.out.println(name + " terminating...");
    }

    private void printNetAmounts() {

        System.out.println("=============================");

        if (isRunning()) {
            System.out.println("The current Net Amounts are: ");
        } else {
            System.out.println("The final Net Amounts are: ");
        }

        netAmounts.forEach((ccy, netAmount) -> {
            if (((BigDecimal) netAmount).compareTo(BigDecimal.ZERO) != 0) {
                System.out.println(((Currency) ccy).getCurrencyCode() + "  " + netAmount + " (USD " + ((BigDecimal) netAmount).divide(new BigDecimal(((Currency) ccy).getFxRate()), 2, RoundingMode.HALF_UP) + ")");
            }
        });

        System.out.println("=============================");
    }
}
