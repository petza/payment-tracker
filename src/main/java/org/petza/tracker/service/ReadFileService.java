package org.petza.tracker.service;

import org.petza.tracker.domain.NetAmounts;
import org.petza.tracker.domain.Payment;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Scanner;


/** Assumes UTF-8 encoding. JDK 7+. */
public class ReadFileService {

    private final Path fFilePath;
    private NetAmounts netAmounts;
    private Map<String, Double> currencyUSDFxRates;
    private ReadDataService readDataService;
    private final static Charset ENCODING = StandardCharsets.UTF_8;

    /**
     Constructor.
     * @param filePath full name of an existing, readable file.
     * @param currencyUSDFxRates
     */
    public ReadFileService(String filePath, NetAmounts netAmounts, Map<String, Double> currencyUSDFxRates){
        this.fFilePath = Paths.get(filePath);
        this.netAmounts = netAmounts;
        this.currencyUSDFxRates = currencyUSDFxRates;

        this.readDataService = new ReadDataService(this.netAmounts, this.currencyUSDFxRates,"readDataService");
    }


    /** Template method that calls {@link #processLine(String)}.  */
    public final void processLineByLine() throws IOException {
        try (Scanner scanner =  new Scanner(fFilePath, ENCODING.name())){
            while (scanner.hasNextLine()){
                processLine(scanner.nextLine());
            }
        }
    }

    /**
     Overridable method for processing lines in different ways.

     <P>This simple default implementation expects simple name-value pairs, separated by an
     ' '. Examples of valid input:
     <tt>CZK 100</tt>
     <tt>EUR 200</tt>
     <tt>USD 300</tt>
     <tt>this is the currency = this is the amount</tt>
     */
    protected void processLine(String aLine){

        //new Scanner to parse the content of each line
        Scanner scanner = new Scanner(aLine).useDelimiter("\\s");

        Payment payment = readDataService.readPayment(scanner);
        readDataService.storePayment(payment);
    }
}
