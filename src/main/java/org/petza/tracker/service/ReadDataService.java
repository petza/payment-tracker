package org.petza.tracker.service;

import org.petza.tracker.domain.Currency;
import org.petza.tracker.domain.NetAmounts;
import org.petza.tracker.domain.Payment;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class ReadDataService implements Runnable {

    private Thread readDataThread;

    private String              name;
    private NetAmounts          netAmounts;
    private Map<String, Double> currencyUSDFxRates;
    private final AtomicBoolean running = new AtomicBoolean(true);

    private NetAmountsService netAmountsService;

    private final String QUIT = "QUIT";

    public ReadDataService(NetAmounts netAmounts, String name) {
        this.name = name;
        this.netAmounts = netAmounts;
    }

    public ReadDataService(NetAmounts netAmounts, Map currencyUSDFxRates, String name)
    {
        this.name = name;
        this.netAmounts = netAmounts;
        this.currencyUSDFxRates = currencyUSDFxRates;
    }

    public ReadDataService(NetAmounts netAmounts, Map currencyUSDFxRates, String name, NetAmountsService netAmountsService)
    {
        this.name = name;
        this.netAmounts = netAmounts;
        this.currencyUSDFxRates = currencyUSDFxRates;
        this.netAmountsService = netAmountsService;
    }

    public void start() {
        readDataThread = new Thread(this);
        readDataThread.start();
    }

    public void stop() {
        running.set(false);
    }

    public void interrupt() {
        running.set(false);
        readDataThread.interrupt();
    }

    boolean isRunning() {
        return running.get();
    }

    public Payment readPayment(Scanner scanner) {

        if (scanner.hasNext()) {

            String currencyCode = scanner.next().trim().toUpperCase();
            if (QUIT.equals(currencyCode)) {
                System.out.println("You entered QUIT! Quit the processing...");
                return null;
            }
            if (currencyCode.length() != 3) {
                System.out.println("You entered wrong currency code format! Quit the processing...");
                return null;
            }

            Currency currency = new Currency();
            currency.setCurrencyCode(currencyCode);
            if (currencyUSDFxRates != null && currencyUSDFxRates.containsKey(currencyCode)) {
                currency.setFxRate(currencyUSDFxRates.get(currencyCode));
            } else {
                currency.setFxRate(1d);
            }

            Payment payment = new Payment();
            payment.setCurrency(currency);
            try {
                BigDecimal amount = scanner.nextBigDecimal();
                payment.setAmount(amount);
            } catch (InputMismatchException exception) {
                System.out.println("You entered wrong amount! Must be number! Quit the processing...");
                return null;
            }

            System.out.println("Incoming payment: currency = " + payment.getCurrency().getCurrencyCode() + ", amount = " + payment.getAmount());
            return payment;
        }
        return null;
    }

    public synchronized void storePayment(Payment payment) {
        if (netAmounts.containsKey(payment.getCurrency())) {
            BigDecimal currentAmount = (BigDecimal) netAmounts.get(payment.getCurrency());
            netAmounts.replace(payment.getCurrency(), currentAmount.add(payment.getAmount()));
        } else {
            netAmounts.put(payment.getCurrency(), payment.getAmount());
        }
    }

    @Override
    public void run() {

//        System.out.println(name + " starting.");

        while (isRunning()) {

            System.out.println("Enter a currency code \"XXX\" and amount: ");

            Scanner scanner = new Scanner(System.in).useDelimiter("\\s");
            Payment payment = readPayment(scanner);

            if (payment != null) {
                storePayment(payment);
            } else {
                netAmountsService.interrupt();
                stop();
            }
        }

        System.out.println(name + " terminating...");
    }
}
